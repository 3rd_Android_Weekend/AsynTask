package com.kshrd.asyntask;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new MyTask().execute("http://110.74.194.125:1301/v1/api/articles?page=1&limit=15");

    }

    class MyTask extends AsyncTask<String, Integer, String> {

        private static final String TAG = "ooooo";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.e(TAG, "OnPreExecute");
        }

        @Override
        protected String doInBackground(String... params) {

            String result = "";
            try {
                URL url = new URL(params[0]);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("GET");

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));

                int c;
                while ((c = bufferedReader.read()) != -1) {
                    result += String.valueOf((char) c);
                }

                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject object = new JSONObject(result);
                JSONArray data = object.getJSONArray("DATA");
                object = data.getJSONObject(1);
                object = object.getJSONObject("CATEGORY");
                Log.e(TAG, object.getInt("ID") + "");


                //Log.e(TAG, object.getString("TITLE"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
}
